/*

UXN assembler for Javascript by Nate Cull

v0.01 2023-05-21

Derived and modified from
https://git.sr.ht/~rabbits/uxn/blob/main/src/uxnasm.c
with the following copyright notice

Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.

*/

"use strict"

// Namespace ua for Uxn Assembler

const ua = {}

// Constants

ua.TRIM = 0x100
ua.LENGTH = 0x10000
ua.OPCODES = 0x20
ua.KBIT = 1 << 7
ua.RBIT = 1 << 6
ua.SBIT = 1 << 5

// Prototypes as an approximation of type definitions

ua.Macro = function Macro () {
	this.name = ""
	this.items = []
}

ua.Label = function Label () {
	this.name = ""
	this.addr = 0
	this.refs = 0
}

ua.Reference = function Reference () {
	this.name = ""
	this.rune = ""
	this.addr = 0
}

// Program has a reset method so we can interactively clear it
// while debugging without breaking the global 'p' variable

ua.Program = function Program () {
	this.reset = function () {
		this.data = new Uint8Array(ua.LENGTH)
		this.ptr = 0
		this.length = 0
		this.labels = []
		this.macros = []
		this.refs = []
		this.scope = ""
		this.commentdepth = 0 // comment parsing mode
		this.macro = "" // macro defining mode
		this.macrodef = []
	}
	this.reset()
}

ua.p = new ua.Program
const p = ua.p // shortcut for ease of porting the C code



ua.ops = [
	"LIT", "INC", "POP", "NIP", "SWP", "ROT", "DUP", "OVR",
	"EQU", "NEQ", "GTH", "LTH", "JMP", "JCN", "JSR", "STH",
	"LDZ", "STZ", "LDR", "STR", "LDA", "STA", "DEI", "DEO",
	"ADD", "SUB", "MUL", "DIV", "AND", "ORA", "EOR", "SFT"
]


	
// check if string is lower-case hexadecimal

ua.ishex = function (s) { 
	for (const c of s) {
		if (! (c >= "0" && c <= "9") && ! (c >= "a" && c <= "f")) {
				return false
		}
	}
	return true
}

// convert hex string to number

ua.hextonum = function (s) { 
		return Number.parseInt(s,16).valueOf() 
}

ua.numtohex2 = function (n) { 
		return (n).toString(16).padStart(2,"0")
}

ua.numtohex4 = function (n) { 
		return (n).toString(16).padStart(4,"0")
}




// Functions

// Tools for converting ints and shorts
// These all operate on Javascript Numbers

ua.unsignint8 = n => n >=0 ? n : 256 + n
ua.unsignint16 = n => n >= 0 ? n : 65536 + n
ua.hibyte = n => n >> 8
ua.lobyte = n => n & 0xff

// Create a sublabel out of a Scope and a Name

ua.sublabel = function(scope,name) {
	return scope + "/" + name
}

// Look up a macro by name

ua.findmacro = function(name) {
	return p.macros.find(m => m.name == name)
}

// Look up a label by name

ua.findlabel = function (name) {
	return p.labels.find(l => l.name == name)
}

// Convert an opcode string mnemonic into an opcode integer


ua.findopcode  = function (s) {
	const base = s.slice(0,3)
	
	// Special opcode 0 variants
	
	if (base == "BRK") { return 0 }
	if (base == "JCI") { return 0x20 }
	if (base == "JMI") { return 0x40 }
	if (base == "JSI") { return 0x60 }
	
	// Otherwise a normal opcode
	
	const ext = s.slice(3)
	let i = ua.ops.indexOf(base)
	
	// -1 means base opcode wasn't found
	if (i < 0) { return i}
	
	if (i == 0) { i = i | ua.KBIT } // LIT has KEEP bit
	
	for (const c of ext) {
		if (c == '2') { i = i | ua.SBIT}
		if (c == 'r') { i = i | ua.RBIT}
		if (c == 'k') { i = i | ua.KBIT}
	}
	
	return i
}

// Create a new macro definition


ua.makemacro = function (name) {
	//console.log("Macro: " + name)
	if (ua.findmacro(name)) {
		throw "Macro duplicate: " + name
	}
	if (ua.ishex(name)) { 
		throw "Macro name is hex number: " + name
	}
	if (ua.findopcode(name) >= 0) {
		throw "Macro name is invalid: " + name
	}
	const m = new ua.Macro
	p.macros.push(m)
	m.name = name

}

ua.makelabel = function (name) {
	console.log("Label:  " + ua.numtohex4(p.ptr) + " " + name)
	const l = new ua.Label
	if (ua.findlabel(name)) { 
		throw "Label duplicate: " + name
	}
	if (ua.ishex(name) || name.len == 2 && name.len == 4) {
		throw "Label is hex number: " + name
	}
	if (name.length < 1 || ua.findopcode(name) >=0 ) {
		throw "Label name is invalid: " + name
	}
	p.labels.push(l)
	l.addr = p.ptr
	l.refs = 0
	l.name = name
	
}

ua.makereference = function (scope, label, rune, addr) {
	console.log("Ref:    " + ua.numtohex4(addr) + " " + scope + " " + rune + " " + label)
	const r = new ua.Reference
	p.refs.push(r)
	if (label[0] == "&") {
		const subw = ua.sublabel(scope,label.slice(1))
		if (!subw) { throw "Invalid sublabel: " + label}
		r.name = subw
	} else {
		const slashpos = label.indexOf("/")
		if (slashpos >=0) { 
			const par = label.slice(0,slashpos)
			const l = ua.findlabel(par)
			if (l) { l.refs++ }
		}
		r.name = label
	}
	r.rune = rune
	r.addr = addr
}

ua.writeopcode = function(w) {
	const opbyte = ua.findopcode(w)
	if (opbyte < 0) { throw "Invalid opcode to write: " + w }
	console.log("Opcode: " + ua.numtohex4(p.ptr) + " " + ua.numtohex2(opbyte) + " " + w )
	ua.writebyte(opbyte)
	
}

ua.writebyte = function(b) {
	// console.log("Byte:   " + ua.numtohex4(p.ptr) + " " + ua.numtohex2(b) )
	if (p.ptr < ua.TRIM) { throw "Writing in zero-page: " + p.ptr.toString(16) }
	if (p.ptr >= ua.LENGTH) { throw "Writing after the end of RAM: " + p.ptr.toString(16) }
	if (p.ptr < p.length) { throw "Memory overwrite: " + p.ptr.toString(16) }
	p.data[p.ptr++] = b
	p.length = p.ptr
}

ua.writelitbyte = function(b) {
	ua.writeopcode("LIT")
	ua.writebyte(b)
}

ua.writeshort = function(s) {
	ua.writebyte(ua.hibyte(s))
	ua.writebyte(ua.lobyte(s))
}

ua.writelitshort = function(s) {
	ua.writeopcode("LIT2")
	ua.writeshort(s)
}

// Process an include

ua.doinclude = function (fname) {
	console.log("Including: " + filename)
	ua.parsefile(fname)
	
}

// parse a word

ua.parse = function (w) {
	
	//console.log("Word: " + w)
	
	// Comments
	
	if (w == "(") { 
		p.commentdepth++
		return
	}
	
	if (w == ")") {
		if (p.commentdepth > 0) { p.commentdepth-- }
		return
	}
	
	if (p.commentdepth > 0) { 
		// console.log("Comment: " + w)
		return 
	}
	
	
	
	// Macros
	
	if (p.macro.length > 0) {
		if (w == "{") { return }
		if (w == "}") { 
			console.log("Macro:  " + p.macro + " { " + p.macrodef.join(" ") + " }")

			const m = ua.findmacro(p.macro)
			m.items = p.macrodef
			p.macrodef = []
			p.macro = ""
			return
		}
		p.macrodef.push(w)
		return
	}
	
	// Normal handling
	
	let wrune = w[0]
	let wtext = w.slice(1)
	let subw = ""
	let l = null

	switch(wrune) {
	case "(": // comment 
		p.commentdepth++
		break
	case ")": // end comment
		if (p.commentdepth > 0) { p.commentdepth-- }
		break
	case "~": // include
		ua.doinclude(wtext)
		break
	case "%": // macro definition
		ua.makemacro(wtext)
		p.macro = wtext
		p.macrodef = []
		break

	case "|": // pad-absolute
		if (ua.ishex(wtext)) {
			p.ptr = ua.hextonum(wtext)
		} else if (wtext[0] == "&") {
			subw = ua.sublabel(p.scope,wtext.slice(1))
			l = ua.findlabel(subw)
			if (!l) { throw "Invalid sublabel: " + w }
			p.ptr = l.addr
		} else {
			l = ua.findlabel(wtext)
			if (!l) { throw "Invalid sublabel: " + w }
			p.ptr = l.addr
		}
		break
	case "$": // pad-relative
	if (ua.ishex(wtext)) {
			p.ptr += ua.hextonum(wtext)
		} else if (wtext[0] == "&") {
			subw = ua.sublabel(p.scope,wtext.slice(1))
			l = ua.findlabel(subw)
			if (!l) { throw "Invalid sublabel: " + w }
			p.ptr += l.addr
		} else {
			l = ua.findlabel(wtext)
			if (!l) { throw "Invalid sublabel: " + w }
			p.ptr += l.addr
		}
		break
	case "@": // label
		ua.makelabel(wtext)
		p.scope = wtext
		break
	case "&": // sublabel
		subw = ua.sublabel(p.scope,wtext)
		ua.makelabel(subw)
		break
	case "#": // literal hex
		if (ua.ishex(wtext)) {
			const num = ua.hextonum(wtext)
			if (wtext.length == 2) { 
				ua.writelitbyte(num)
				break
			}
			if (wtext.length == 4) {
				ua.writelitshort(num)
				break
			}
		}
		throw "Invalid hex literal: " + w
		break
		
	case "_": // raw byte relative
		ua.makereference(p.scope,wtext,wrune,p.ptr)
		ua.writebyte(0xff)
		break
	case ",": // literal byte relative
		ua.makereference(p.scope,wtext,wrune,p.ptr+1)
		ua.writelitbyte(0xff)
		break
	case "-": // raw byte absolute
		ua.makereference(p.scope,wtext,wrune,p.ptr)
		ua.writebyte(0xff)
		break
	case ".": // literal byte zero-page
		ua.makereference(p.scope,wtext,wrune,p.ptr+1)
		ua.writelitbyte(0xff)
		break
	case ":":
	case "=": // raw short absolute
		ua.makereference(p.scope,wtext,wrune,p.ptr+1)
		ua.writeshort(0xffff)
		break
	case ";": // literal short absolute
		ua.makereference(p.scope,wtext,wrune,p.ptr+1)
		ua.writelitshort(0xffff)
		break
	case "?": // JCI jump conditional immediate
		ua.makereference(p.scope,wtext,wrune,p.ptr+1)
		ua.writeopcode("JCI")
		ua.writeshort(0xffff)
		break
	case '!': // JMI jump immediate
		ua.makereference(p.scope,wtext,wrune,p.ptr+1)
		ua.writeopcode("JMI")
		ua.writeshort(0xffff)
		break
	case '"': // raw string, ASCII only, no final 0x00
		console.log("String: " + ua.numtohex4(p.ptr) + " " + wtext)
		for (const c of wtext) {
			ua.writebyte(c.charCodeAt(0) & 0xff)
		}
		break
	case "[":
	case "]":
		if (wtext == "") { break }
	default:
		// opcode
		
		if (ua.findopcode(w) >= 0) {
			ua.writeopcode(w)
			return
		}
		
		// raw hex byte
		
		if (ua.ishex(w) && w.length == 2) {
			ua.writebyte(ua.hextonum(w))
			return
		}
		
		// raw hex short
		
		if (ua.ishex(w) && w.length == 4) {
			ua.writeshort(ua.hextonum(w))
			return
		}
		
		// macro
		
		const m = ua.findmacro(w)
		
				
		if (m) {
			console.log("Mcall:  " + ua.numtohex4(p.ptr) + " " + w)

			for (const x of m.items) { 
				ua.parse(x)
			}
			
			console.log("Mdone:  " + ua.numtohex4(p.ptr) + " " + w)

			return
		}
		
		// final default for an unknown word is JSI
		// ie a subroutine call
		
		ua.makereference(p.scope, w, ' ', p.ptr + 1)
		ua.writeopcode("JSI")
		ua.writeshort(0xffff)
		
	}
	
	
}



ua.resolve = function() {
	console.log("Resolving " + p.refs.length + " references")
	let l
	let reladdr
	let addr
	for (const r of p.refs) {
		switch (r.rune) {
			case "_":
			case ",":
				l = ua.findlabel(r.name)
				if (!l) { throw "Unknown relative byte reference: " + r.name }
				reladdr = (l.addr - r.addr - 2)
				if (reladdr > 127 || reladdr < -128) {
					throw "Relative byte reference is too far: " + r.name + " " + reladdr
				}
				addr = ua.unsignint8(reladdr)
				
				console.log(ua.numtohex4(r.addr) + "      " + ua.numtohex2(addr) + " " + r.rune + " " + r.name + " (" + reladdr +")")
				
				p.data[r.addr] = addr
				l.refs++
				break
			case "-":
			case ".":
				l = ua.findlabel(r.name)
				if (!l) { throw "Unknown zero-page reference: " + r }
				
				console.log(ua.numtohex4(r.addr) + "      " + ua.numtohex2(l.addr) + " " + r.rune + " " + r.name )
				
				p.data[r.addr] = ua.lobyte(l.addr)
				l.refs++
				break
			case ":":
			case "=":
			case ";":
				l = ua.findlabel(r.name)
				if (!l) { throw "Unknown absolute reference: " + r.name }
				
				console.log(ua.numtohex4(r.addr) + "    " + ua.numtohex4(l.addr) + " " + r.rune + " " + r.name )
				
				p.data[r.addr] = ua.hibyte(l.addr)
				p.data[r.addr+1] = ua.lobyte(l.addr)
				l.refs++
				break
			case "?":
			case "!":
			default:
				l = ua.findlabel(r.name)
				if (!l) { throw "Unknown relative short reference:" + r.name }
				reladdr = l.addr - r.addr - 2
				if (reladdr > 32767 || reladdr < -32768) {
					throw "Relative short reference is too far: " + r.name + " " + reladdr
				}
				addr = ua.unsignint16(reladdr)
				
				console.log(ua.numtohex4(r.addr) + "    " + ua.numtohex4(addr) + " " + r.rune + " " + r.name + " (" + reladdr +")")
				
				p.data[r.addr] = ua.hibyte(addr)
				p.data[r.addr+1] = ua.lobyte(addr)
				l.refs++
				break
		}
	}
	
}

// Cheap and dirty file parsing:
// just read the whole thing into memory as a string
// This should be good enough for files on the order of 64K

ua.parsefile = function(fname) {
	console.log("Parsing file: " + fname)
	const fstring = fs.readFileSync(fname,"utf8")
	ua.parsestring(fstring)
}


ua.iswhitespace = function(c) {
	return 	c == " " || 
			c == "\t" ||
			c == "\n" ||
			c == "\r"
}
ua.parsestring = function(s) {
	let w = ""
	for (const c of s) {
		if (ua.iswhitespace(c)) {
			if (w.length > 0) { ua.parse(w) }
			w = ""
		} else {
			w += c
		}
	}
	if (w.length > 0) { ua.parse(w) }
}

ua.dumprom = function(rom) {
	let i
	let s = ""
	for (i=0;i<rom.length;i++) {
		if (i % 0x10 == 0) {
			console.log(s)
			s = ua.numtohex4(i + ua.TRIM) + " "
		} 
		
		s+= ua.numtohex2(rom[i]) + " "
		
	}
	
	console.log(s)
}

ua.writerom = function (f) {
	const rom = p.data.slice(ua.TRIM,p.length)
	
	console.log("Writing " + rom.length + " bytes to: " + f)

	fs.writeFileSync(f,rom)
	
	ua.dumprom(rom)
	
}	
	
ua.assemble = function (fname) {
	// You get one filename and mandatory extensions sorry
	
	const fin = fname + ".tal"
	const fout = fname + ".rom"
	
	console.log("Assembling: " + fin + " to: " + fout)
	p.reset()
	p.ptr = ua.TRIM // Begin assembling from page 1
	p.scope = "on-reset"
	ua.parsefile(fin) // Parse each word in the file
	ua.resolve() // Go back and resolve references
	ua.writerom(fout)
}


module.exports = ua
