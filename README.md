# uxn

Toys for the Uxn virtual machine, which I did not write but can be found online eg here:

https://wiki.xxiivv.com/site/uxn.html 

https://github.com/hundredrabbits/awesome-uxn

Uxn is a "fantasy CPU" heavily inspired by the 6502 but with a Forth stack machine flavour. I did not design it but it seems pretty good for its specifications: 8/16 bits and 64K max RAM. I think it's probably better for software experimentation than CollapseOS (which targets actually-existing Z80s) and more efficient on small machines than any of the flavours of Retro Forth, which are word based and so waste a large amount of RAM. 

Things I really, really like about Uxn: it has complete symmetry between the Working and Return stacks, and complete symmetry between byte and 16-bit short-int operations. And both stacks are *completely* untyped: you can mix bytes and shorts and jump to either of them with random precision. The resulting devastating footguns still can't be any worse than a standard Forth, plus you get the delicious sense of power from shaving entire bytes from your code exactly as and when you want to.

Things I don't like: no block device and no support for more than 64K, and a main focus on sound and graphics applications. Also, it's a conventional von Neumann machine code CPU with no memory protection. Simple and practical for emulation, certainly. Is the "unprotected VMs, but lots of them" approach useful for long-term software preservation given that RAM corruption endemic to the conventional von Neumann machine architecture is the number one cybersecurity problem on the planet? We'll see.

Varvara is a "fantasy 1980s home computer" specification for the Uxn CPU, apparently somewhere between the Sinclair ZX Spectrum and BBC Micro in capabilities. A text console, one graphics mode (bitmapped 2-bit screen with 4 selectable colours), four-channel bitmap audio synthesizer with ADSR envelopes, file I/O from the host OS, and mouse, keyboard and gamepad input. Most Uxn emulators seem to target Varvara. I did not design Varvara either.

Tal is an assembly language for Uxn machine code. Since all Uxn opcodes are exactly one byte with no registers, it's very simple and word (space-separated-token) oriented. Like Retro Forth, Tal uses single special character prefixes ("runes", equivalent to Retro's "sigils") to define operations. Unlike Retro, Tal runs offline and generates a ROM file, with all its symbols outside of the ROM, because it's an assembly language, not a Forth. But once you've defined some macros and subroutines, Tal code will start to look very much like Retro code because the Uxn processor is pretty much a Forth machine and because of all the runes/sigils. I did not design Tal either.

Uxnasm is an assembler for Tal.


Toys currently online:

## uxnasm.js

A fairly close Javascript (Node) port of uxnasm.c from  https://git.sr.ht/~rabbits/uxn/tree/main/item/src/uxnasm.c

I wrote uxnasm.js because just getting to a hello world C build environment on Windows 10 using MSYS2 and Cygwin is a thermonuclear OMG NOPE on the "will this brick my laptop" software complexity scale, and frankly more than a little bit silly since the whole point of ultra-minimalist tiny VMs like Uxn, other than aesthetics, is to be simple and portable.

Meanwhile Node is one .EXE file and just works on Windows and everyone's antivirus scanner is okay with it. 

There was another attempted Javascript port of uxnasm (again not by me) which has been abandoned for two years and does not work. https://github.com/rafapaezbas/uxnasm-js

My uxnasm.js currently has no test suite but it seems to be working so far. I have been testing it by running ROMs on uxn32 from https://github.com/randrew/uxn32 I have not tested generated ROMs on any other emulator.

### Usage (from Node interactive command shell)

`ua = require("./uxnasm.js")`

`ua.assemble(filename)`

Filename must have no extension. The .tal file will be read and the .rom file written. Unlike the C version, there will be a somewhat excessive amount of screen output.


